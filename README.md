# Archivo README.md

**Versión 1.0**

Archivo README.md para laboratorio número 3 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 1/10/2021

---
## Resúmen del programa

Estos programas fueron desarrollados por Benjamín Fisher, para el almacenamiento de datos en memoria dinámica.

---
## Requerimientos

Para utilizar estos programas se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable de los programas.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/LAB_3_U1.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe escoger el programa que desea utilizar, por ejemplo si quiere utilizar el codigo 1 usted debe ingresar a la carpeta 1 y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

El mismo proceso se debe seguir para poder utilizar los otros dos codigos.

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar los programas!

---
## Funcionamiento del programa

Codigo 1: El primer programa nos permite ingresar n cantidad de numeros enteros (positivos y negativos) a una lista y luego separar la lista en dos, una de numeros positivos (mas el cero) y otra de numeros negativos. El codigo esta estructurado en base a una función main, la cual mantiene el orden de compilacion y ejecucion. Además de esta función, se utilizo una clase llamada "Lista", la cual nos permite crear nodos con informacion numerica, ordenarlos por tamaño y separar la lista en dos sublistas.

Codigo 2: El segundo programa nos permite ingresar n cantidad de "strings" y ordenarlos por orden alfabetico. Se utilizo el mismo codigo del ejercicio anterior, solo que se cambiaron algunas variables de "int" a "string" y se utilizo la librería <algorythm>, especificamente la funcion "transform()" la cual nos permite convertir cualquier string a minuscula. Esto se implemento para eliminar el error que surgia al comparar dos caracteres iguales, pero uno mayúscula y el otro minuscula (Una letra tiene mayúscula y minuscula, en el codigo ASCII tienen valores distintos a pesar de ser la misma letra).

Codigo 3: El tercer programa nos permite crear una lista de postres ordenados alfabeticamente con sus ingredientes. La diferencia con los programas anteriores, es que ahora trabajamos con listas de nodos anidados. Primero de crea un nodo de tipo "Postre", y a este se le asigna un nodo de tipo "Ingrediente". Luego se agrega el nodo postre a una lista y se le asigna como puntero NULL. A medida que el usuario vaya agregando mas postres la liga (conección de cada nodo) va a ir cambiando según su orden alfabetico.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

