#include <iostream>
using namespace std;

#include "Lista.h"

//Funcion que sirve para limpiar la pantalla, no influye en nada
//del funcionamiento del codigo, es mas algo estetico.
void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__Apple__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

//Funcion que nos pemite crear una lista y acceder al menu.
void crea_lista(Lista *lista){
	int opc, num_temp;

		//Ciclo principal
		while(opc != 0){
			cout << "[1] Agregar numero" << endl <<
					  "[2] Separar lista" << endl <<
					  "[3] Salir" << endl <<
					  "Opcion: ";
			cin >> opc;
			
			//Ingresa numeros.
			if(opc == 1){
				cout << "Ingrese el numero: ";
				cin >> num_temp;
				cout << endl;
				lista->agregar(num_temp);
				lista->imprimir();
			}
			
			//Separa la lista en dos, una positiva y la
			//otra negativa, ambas ordenadas.
			else if (opc == 2){
				opc = 0;
			}
			
			else{
				cout << "Opcion invalida..." << endl;
			}
		}
}

//FUNCION MAIN
int main (void) {
	Lista *lista = new Lista();
	Lista *lista_positiva = new Lista();
	Lista *lista_negativa = new Lista();
	
	cout << "Bienvenido!" << endl;
	crea_lista(lista);
	clear();
	
	
	//TRATAR DE PASAR A FUNCION!!
	Nodo *q;
	q = lista->principal;
	
	while(q != NULL){
		//Si el numero es positivo.
		if(q->numero >= 0){
			lista_positiva->agregar(q->numero);
		}
		
		//Si el numero es negativo.
		else{
			lista_negativa->agregar(q->numero);
		}
		
		q = q->sig;
	}
	cout << "LISTA NEGATIVA: " << endl;
	lista_negativa->imprimir();
	cout << endl << "LISTA POSITIVA: " << endl;
	lista_positiva->imprimir();
    return 0;
}
