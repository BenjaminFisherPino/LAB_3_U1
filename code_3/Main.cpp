#include <iostream>
using namespace std;

#include "Lista.h"


//Funcion que sirve para limpiar la pantalla, no influye en nada
//del funcionamiento del codigo, es mas algo estetico.
void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__Apple__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

//Funcion que nos pemite crear una lista y acceder al menu.
void crea_lista(Lista *postres){
	int opc;
	string string_temp;
	opc = 1;
		//Ciclo principal
		while(opc != 0){
			cout << "[1] Agregar Postre" << endl <<
					"[2] Ver postres" << endl <<
					"[3] Salir" << endl <<
					"Opcion: ";
			cin >> opc;
			
			//Ingresa postre.
			if(opc == 1){
				clear();
				cout << "Ingrese el nombre del postre: ";
				cin >> string_temp;
				cout << endl;
				postres->agregar_postre(string_temp);
				clear();
			}
			
			//Ver los postres disponibles.
			else if (opc == 2){
				clear();
				postres->imprimir_postres();
				cout << "Desea ver los ingredientes de un postre?" << endl;
				cin >> opc;
				
				//Ver ingredientes
				if (opc == 1){
					Lista *q;
					q = postres;
					int temp, cont;
					cont = 1;
					q->imprimir_postres();
					cout << "Ingrese el indice del postre" << endl;
					cin >> temp;
					q->agregar_ingrediente(temp);
				}else{}
			}
			
			//Salir del programa.
			else if (opc == 3){
				opc = 0;
			}
			
			else if ((opc !=1)&&(opc !=2)&&(opc !=3)&&(opc !=4)){
				cout << "Opcion invalida..." << endl;
			}
		}
}


//FUNCION MAIN
int main (void) {
	Lista *postres = new Lista();
	crea_lista(postres);
    return 0;
}
