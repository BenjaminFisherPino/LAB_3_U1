#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO (INGREDIENTE)
typedef struct _Ingrediente {
	string nombre;
	struct _Ingrediente *sig;
} Ingrediente;

//ESTRUCTURA DEL NODO (POSTRE)
typedef struct _Postre {
    string nombre;
    struct _Postre *sig;
    Ingrediente *ingredientes;
} Postre;

class Lista {

    public:
		Postre *principal = NULL;
		Ingrediente *iprincipal = NULL;
		
        //CONSTRUCTOR POR DEFECTO
        Lista();
        
        //Se crea un nodo
        void agregar_postre(string a);
        
        //Se crea un nodo 
        void agregar_ingrediente(int a);
        
        //Imprime la lista de postres actual.
        void imprimir_postres();
        
        void imprimir_ingredientes();
        
        void ver_ingredientes(int a);
};
#endif
