#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista() {}

void Lista::agregar_postre(string a) {
    Postre *q;

    //Se crea un nodo.
    q = new Postre;
    
    //Se asigna el nodo al objeto numero.
    q->nombre = a;
    
    //Se inicializa como NULL por defecto.
    q->sig = NULL;

    /*Si el es primer nodo de la lista, lo deja
    como raíz y como último nodo.*/
    if (this->principal == NULL) { 
		this->principal = q;

    /*De lo contrario, apunta el actual último nodo 
    al nuevo y deja el nuevo como el último de la lista. */
    }
    
    else {
		Postre *s, *r;
		s = this->principal;
		
		while ((s != NULL) && (s->nombre < a)){
			r = s;
			s = s->sig;
		}
		
		if (s == this->principal){
			this->principal = q;
			this->principal->sig = s;
		}
		
		else{
			r->sig = q;
			q->sig = s;
		}
    }
}

void Lista::agregar_ingrediente(int a){
	int cont;
	string nombre;
	Postre *q = this->principal;
	Ingrediente *i;
	i = new Ingrediente();
	cout << "Ingrese el nombre del ingrediente" << endl;
	cin >> nombre;
	i->nombre = nombre;
	i->sig = NULL;
	
	//Aqui se busca el postre deseado
	while ((q != NULL) && (cont <= a)){
		cont++;
		q = q->sig;
	}
	
	q->ingredientes = i;
	
	if (this->iprincipal == NULL){
		this->iprincipal = i;
	}else{
		i->sig = i;
	}
	
	Ingrediente *ingre = NULL;
	
	ingre = this->iprincipal;
	
	while (ingre != NULL){
		cout << ingre->nombre << endl;
		ingre = ingre->sig;
	}
}

void Lista::imprimir_postres() {
	
    //Utiliza variable temporal para recorrer la lista.
    Postre *q = this->principal;
    
    //Contador
	int i = 1;
	
    //La recorre mientras sea distinto de NULL (no hay más nodos).
    while (q != NULL) {
        cout <<  i << ") " << q->nombre << endl;
        i++;
        q = q->sig;
    }
    cout << endl;
}
