#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista() {}

void Lista::agregar(string a) {
    Nodo *q;

    //Se crea un nodo.
    q = new Nodo;

    //Se asigna el nodo al objeto numero.
    q->palabra = a;
    
    //Se inicializa como NULL por defecto.
    q->sig = NULL;

    /*Si el es primer nodo de la lista, lo deja
    como raíz y como último nodo.*/
    if (this->principal == NULL) { 
		this->principal = q;

    /*De lo contrario, apunta el actual último nodo 
    al nuevo y deja el nuevo como el último de la lista. */
    }
    
    else {
		Nodo *s, *r, tmp;
		s = this->principal;
		
		while ((s != NULL) && (s->palabra < a)){
			r = s;
			s = s->sig;
		}
		
		if (s == this->principal){
			this->principal = q;
			this->principal->sig = s;
		}
		
		else{
			r->sig = q;
			q->sig = s;
		}
    }
}

void Lista::imprimir () {
	
    //Utiliza variable temporal para recorrer la lista.
    Nodo *q = this->principal;
    
    //Contador
	int i = 1;
	
    //La recorre mientras sea distinto de NULL (no hay más nodos).
    while (q != NULL) {
        cout <<  i << ") " << q->palabra << endl;
        i++;
        q = q->sig;
    }
    cout << endl;
}
