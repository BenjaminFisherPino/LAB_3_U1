#include <iostream>
#include <algorithm>
using namespace std;

#include "Lista.h"

//Funcion que sirve para limpiar la pantalla, no influye en nada
//del funcionamiento del codigo, es mas algo estetico.
void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__Apple__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

//Funcion que nos permite crear las listas 1 y 2
void crea_lista(Lista *lista){
	int opc;
	string string_temp;
		while(opc != 0){
		cout << "[1] Agregar palabra" << endl <<
	               "[2] Cerrar programa" << endl <<
	               "Opcion: ";
		cin >> opc;
		
		if(opc == 1){
			cout << "Ingrese la palabra: ";
			cin >> string_temp;
			cout << endl;
			
			transform(string_temp.begin(), string_temp.end(), 
			string_temp.begin(), ::tolower);
			
			lista->agregar(string_temp);
			lista->imprimir();
		}
		
		if(opc == 2){
			opc = 0;
		}
		
		else if((opc != 1) && (opc !=2)){
			cout << "Opcion invalida..." << endl;
		}
	}
}

//FUNCION MAIN
int main (void) {
	Lista *lista = new Lista();
	crea_lista(lista);
	return 0;
}
    

