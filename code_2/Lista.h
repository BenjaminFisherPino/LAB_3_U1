#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO
typedef struct _Nodo {
    string palabra;
    struct _Nodo *sig;
} Nodo;

class Lista {

    public:
		Nodo *principal = NULL;
		
        //CONSTRUCTOR POR DEFECTO
        Lista();
        
        //Se crea un nodo que recibe un dato de tipo string.
        void agregar(string a);
        
        //Imprime la lista actual.
        void imprimir();
};
#endif
